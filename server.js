const https = require('https');

const cors = require('cors');

const fs = require('fs');

const options =  {
    key: fs.readFileSync('/etc/letsencrypt/live/doc.najva.com-0001/privkey.pem'),
    cert: fs.readFileSync('/etc/letsencrypt/live/doc.najva.com-0001/fullchain.pem')
};

var express = require('express'),
    app = express(),
    port = process.env.PORT || 3001,
    bodyParser = require('body-parser');

app.use(cors());

app.use(function(req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', 'https://www.najva.com');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

app.use(express.static(__dirname + '/public'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());


app.listen(port);

https.createServer(options, app).listen(port + 1);

if (!fs.existsSync('simple_api_data.csv')) {
    fs.writeFileSync('simple_api_data.csv', 'first_name,last_name,mobile,email,company,website,category,invitation_code,date\n');
}

if (!fs.existsSync('simple_api_error.csv')) {
    fs.writeFileSync('simple_api_error.csv', 'first_name,last_name,mobile,email,company,website,category,invitation_code,date\n');
}

app.get('/', function(req, res) {
    res.send("hello");
});

var controller = require('./api/controller');

app.route('/create')
    .post(controller.create_a_field);

app.route('/register')
    .post(controller.register_panels);

console.log('todo list RESTful API server started on: ' + port);
