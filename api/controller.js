const request = require('request');
const fs = require('fs');
const readline = require('readline');

const delay = ms => new Promise(res => setTimeout(res, ms));


var domain = function(body, data) {

    post_data = {
        'domain': body.website,
        'email_address': body.email
    }

    console.log("sending domain")

    request.post(
        'https://najv.xyz/api/v2/chapar/domain/', { json: post_data },
        function(error, response, body) {
            if (!error && response.statusCode < 400) {
                console.log(body)
            } else {
                console.log("writing error")
                console.log(body)
                fs.appendFileSync('./simple_api_error.csv', data);
            }
        }
    )
}

var crm = function(body, data) {
    console.log("crm content");

    post_data = {
        'first_name': body.first_name,
        'last_name': body.last_name,
        'email_address': body.email,
        'domain': body.website,
        'company': body.company || 'none',
        'mobile_number': body.mobile
    }
    
    console.log("sending user");
    console.log(post_data);

    return new Promise( (resolve,reject)=> { 
        request.post(
            'https://app.najva.com/api/v2/chapar/user/', { json: post_data },
            function(error, response, response_body) {
                if (!error && response.statusCode < 400) {
                    console.log(response_body);
                } else {
                    console.log("writing error")
                    console.log(response_body)
                    fs.appendFileSync('./simple_api_error.csv', data);
                }
                resolve({ 'status': response.statusCode, 'response': response_body });
            }
        );
      })
}
    


var readLines = async function() {
    fileStream = fs.createReadStream('simple_api_error.csv');
    const rl = readline.createInterface({
        input: fileStream,
        crlfDelay: Infinity
    });
    // Note: we use the crlfDelay option to recognize all instances of CR LF
    // ('\r\n') in input.txt as a single line break.

    var lines = [];

    for await (const line of rl) {
        // Each line in input.txt will be successively available here as `line`.
        lines.push(line.toString());
    }
    return lines;
}

var panels = async function() {
    var lines = await readLines();

    var heads = lines[0].split(',');

    for (i = 1; i < lines.length; i++) {
        var line = lines[i].split(',')
        var body = {
            first_name: line[heads.indexOf('first_name')],
	    last_name: line[heads.indexOf('last_name')],
            mobile: line[heads.indexOf('mobile')],
            email: line[heads.indexOf('email')],
            company: line[heads.indexOf('company')] || 'none',
            website: line[heads.indexOf('website')],
            category: line[heads.indexOf('category')],
            invitation_code: line[heads.indexOf('invitation_code')],
        }


        var data = body.first_name + ',' +
	    body.last_name + ',' +
            body.mobile + ',' +
            body.email + ',' +
            body.company + ',' +
            body.website + ',' +
            body.category + ',' +
            body.invitation_code + ',' +
            new Date().toUTCString() +
            '\n';

        console.log(body)
        console.log(data)

        await crm(body, data);
        delay(5000);
        domain(body, data);
        delay(5000);
    }



}

exports.register_panels = function(req, res) {

    var pass = req.body.pass

    if (pass != "!@#QWEsalam") {
        res.send('{ "code":401, "message":"unauthorized"');
        return
    }

    panels();

    res.send('{ "code":200,"message":"success" }');

}

var register_user =  async function(body, data) {
    return await crm(body, data);
    //delay(5000);
    //domain(body,data);
}

exports.create_a_field = async function(req, res) {
    console.log(req.body)
    var company = req.body.company;
    req.body.first_name = req.body.first_name || req.body.name;
    req.body.last_name = req.body.last_name || req.body.name;
    var data = req.body.first_name + ',' +
        req.body.last_name + ',' +
        req.body.mobile + ',' +
        req.body.email + ',' +
        req.body.company + ',' +
        req.body.website + ',' +
        req.body.category + ',' +
        req.body.invitation_code + ',' +
        new Date().toUTCString() +
        '\n';

    const fs = require('fs');

    fs.appendFileSync('./simple_api_data.csv', data);

    console.log(data)

    response = await crm(req.body, data);

    console.log(response);

    if (response.status < 400) {
        res.send('{ "code":200,"message":"created" }');
    } else {
        body = response.response;

        key = Object.keys(body.errors)[0];
        
        res.send({'status': response.status,'errors': body.errors, 'detail':body.errors[key][0] })
    }
}

