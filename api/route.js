module.exports = function(app) {
    var todoList = require('./controller');

    // todoList Routes
    app.route('/create')
        .post(todoList.create_a_field);

    app.route('/register')
        .get(todoList.register_panels)

};